package fr.ulille.iut.agile;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.agile.beans.Customer;
import fr.ulille.iut.agile.dao.CustomersDao;
import fr.ulille.iut.agile.dto.CustomerCreateDto;
import fr.ulille.iut.agile.dto.CustomerDto;

public class CustomerResourceTest {

	private static final Logger LOGGER = Logger.getLogger(CustomerResourceTest.class.getName());
	private CustomersDao dao;
	private HttpServer server;
	private WebTarget target;

	@Before
	public void setUp() throws Exception {
		BDDFactory.setJdbiForTests();
		// start the server
		server = Main.startServer();
		// create the client
		Client c = ClientBuilder.newClient();

		// uncomment the following line if you want to enable
		// support for JSON in the client (you also have to uncomment
		// dependency on jersey-media-json module in pom.xml and Main.startServer())
		// --
		// c.getConfiguration().enable(new
		// org.glassfish.jersey.media.json.JsonJaxbFeature());

		target = c.target(Main.BASE_URI);
		dao = BDDFactory.buildDao(CustomersDao.class);
		dao.dropCustomersTable();
		dao.createTable();

	}

	@After
	public void tearDown() throws Exception {
		server.stop();
	}

	// @Test
	// public void testGetEmptyList() {
	// Response response = target.path("customers").request().get();

	// // Vérification de la valeur de retour (200)
	// assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

	// // Vérification de la valeur retournée (liste vide)
	// List<CustomerDto> customers;
	// customers = response.readEntity(new GenericType<List<CustomerDto>>() {
	// });

	// assertEquals(0, customers.size());
	// }

	// @Test
	// public void testGetExistingCustomer() {

	// Customer customer = new Customer();
	// customer.setFirstName("Samy");
	// customer.setLastName("Molette");

	// long id = dao.insert(customer.getLastName(), customer.getFirstName());
	// customer.setId(id);

	// Response response = target.path("/customers/" + id).request().get();

	// assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

	// Customer result = Customer.fromDto(response.readEntity(CustomerDto.class));
	// assertEquals(customer, result);
	// }

	// @Test
	// public void testGetNotExistingCustomer() {
	// Response response = target.path("/customers/125").request().get();
	// assertEquals(Response.Status.NOT_FOUND.getStatusCode(),
	// response.getStatus());
	// }

	// //
	// -------------------------------------------------------------------------------------------------------------------------------------------
	// @Test
	// public void testCreateCustomer() {
	// CustomerCreateDto customerCreateDto = new CustomerCreateDto();
	// customerCreateDto.setFirstName("Samy");
	// customerCreateDto.setLastName("Molette");

	// Response response =
	// target.path("/customers").request().post(Entity.json(customerCreateDto));

	// // On vérifie le code de status à 201
	// assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

	// CustomerDto returnedEntity = response.readEntity(CustomerDto.class);

	// // On vérifie que le champ d'entête Location correspond à
	// // l'URI de la nouvelle entité
	// assertEquals(target.path("/customers/" + returnedEntity.getId()).getUri(),
	// response.getLocation());

	// // On vérifie que le nom correspond
	// assertEquals(returnedEntity.getFirstName(),
	// customerCreateDto.getFirstName());
	// assertEquals(returnedEntity.getLastName(), customerCreateDto.getLastName());
	// }

	// @Test
	// public void testCreateSameCustomer() {
	// CustomerCreateDto customerCreateDto = new CustomerCreateDto();
	// customerCreateDto.setFirstName("Samy");
	// customerCreateDto.setLastName("Molette");
	// dao.insert(customerCreateDto.getLastName(),
	// customerCreateDto.getFirstName());

	// Response response =
	// target.path("/customers").request().post(Entity.json(customerCreateDto));

	// assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
	// }

	// @Test
	// public void testCreateCustomerWithoutName() {
	// CustomerCreateDto customerCreateDto = new CustomerCreateDto();

	// Response response =
	// target.path("/customers").request().post(Entity.json(customerCreateDto));

	// assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(),
	// response.getStatus());
	// }

	/*
	 * @Test public void testCreateWithForm() { Form form = new Form();
	 * form.param("firstName", "Titouin"); form.param("lastName", "Gaming");
	 * Entity<Form> formEntity = Entity.entity(form,
	 * MediaType.APPLICATION_FORM_URLENCODED_TYPE); Response response =
	 * target.path("customers").request().post(formEntity);
	 * 
	 * assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
	 * String location = response.getHeaderString("Location"); long id =
	 * Integer.parseInt(location.substring(location.lastIndexOf('/') + 1)); Customer
	 * result = dao.findById(id);
	 * 
	 * assertNotNull(result); }
	 */

}