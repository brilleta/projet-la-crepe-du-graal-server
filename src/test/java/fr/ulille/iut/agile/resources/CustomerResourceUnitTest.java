package fr.ulille.iut.agile.resources;

import fr.ulille.iut.agile.BDDFactory;
import fr.ulille.iut.agile.beans.Customer;
import fr.ulille.iut.agile.dao.CustomersDao;
import fr.ulille.iut.agile.dto.CustomerCreateDto;
import fr.ulille.iut.agile.dto.CustomerDto;
import org.junit.Assert;
import org.junit.Test;

import javax.ws.rs.WebApplicationException;

public class CustomerResourceUnitTest {
    @Test
    public void test_toto() {
        CustomersDao customersDao = initCustomerTable();

        CustomerResource customerResource = new CustomerResource();
        CustomerCreateDto inputCustomer = new CustomerCreateDto();
        inputCustomer.setFirstName("Firstname");
        inputCustomer.setLastName("Lastname");
        inputCustomer.setLocation("Ici");
        inputCustomer.setMail("a@gmail.com");
        inputCustomer.setTelNum("0000000000");
        inputCustomer.setDate("12/15/1222");
        CustomerDto actual = customerResource.createCustomer(inputCustomer);

        Assert.assertEquals("Firstname", actual.getFirstName());

        Customer actualInBdd = customersDao.findById(actual.getId());
        Assert.assertEquals("Firstname", actualInBdd.getFirstName());
    }

    @Test(expected = WebApplicationException.class)
    public void test_should_conflict() {
        initCustomerTable();

        CustomerResource customerResource = new CustomerResource();
        CustomerCreateDto inputCustomer = new CustomerCreateDto();
        inputCustomer.setFirstName("Firstname");
        inputCustomer.setLastName("Lastname");
        inputCustomer.setLocation("Ici");
        inputCustomer.setMail("a@gmail.com");
        inputCustomer.setTelNum("0000000000");
        inputCustomer.setDate("12/15/1222");
        customerResource.createCustomer(inputCustomer);
        customerResource.createCustomer(inputCustomer);

    }

    private CustomersDao initCustomerTable() {
        CustomersDao customersDao = BDDFactory.buildDao(CustomersDao.class);
        customersDao.dropCustomersTable();
        customersDao.createTable();
        return customersDao;
    }
}
