package fr.ulille.iut.agile.beans;

import fr.ulille.iut.agile.dto.CustomerCreateDto;
import fr.ulille.iut.agile.dto.CustomerDto;

public class Customer {
	private long id;
	private String lastName;
	private String firstName;
	private String date;
	private String telNum;
	private String location;
	private String mail;
	private double rating;

	public Customer(String lastName, String firstName) {
		this.lastName = lastName;
		this.firstName = firstName;
	}

	public Customer(String lastName, String firstName, String location) {
		this.lastName = lastName;
		this.firstName = firstName;
		this.location = location;
	}

	public Customer(String lastName, String firstName, String date, String telNum, String location, String mail) {
		this.lastName = lastName;
		this.firstName = firstName;
		this.date = date;
		this.telNum = telNum;
		this.location = location;
		this.mail = mail;
	}

	public Customer() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTelNum() {
		return telNum;
	}

	public void setTelNum(String telNum) {
		this.telNum = telNum;
	}

	public Double getRating() {
		return rating;
	}

	public void setRating(Double rating) {
		this.rating = rating;
	}

	public static CustomerDto toDto(Customer c) {
		CustomerDto dto = new CustomerDto();
		dto.setId(c.getId());
		dto.setLastName(c.getLastName());
		dto.setFirstName(c.getFirstName());
		dto.setLocation(c.getLocation());
		dto.setMail(c.getMail());
		dto.setDate(c.getDate());
		dto.setRating(c.getRating());
		dto.setTelNum(c.getTelNum());
		return dto;
	}

	public static Customer fromDto(CustomerDto dto) {
		Customer c = new Customer();
		c.setId(dto.getId());
		c.setLastName(dto.getLastName());
		c.setFirstName(dto.getFirstName());
		c.setLocation(dto.getLocation());
		c.setMail(dto.getMail());
		c.setDate(dto.getDate());
		c.setTelNum(dto.getTelNum());
		c.setRating(dto.getRating());
		return c;
	}

	public static CustomerCreateDto toCreateDto(Customer customer) {
		CustomerCreateDto dto = new CustomerCreateDto();
		dto.setFirstName(customer.getFirstName());
		dto.setLastName(customer.getLastName());
		dto.setLocation(customer.getLocation());
		dto.setDate(customer.getDate());
		dto.setTelNum(customer.getTelNum());
		dto.setMail(customer.getMail());
		return dto;
	}

	public static Customer fromCustomerCreateDto(CustomerCreateDto dto) {
		Customer customer = new Customer();
		customer.setFirstName(dto.getFirstName());
		customer.setLastName(dto.getLastName());
		customer.setLocation(dto.getLocation());
		customer.setDate(dto.getDate());
		customer.setTelNum(dto.getTelNum());
		customer.setMail(dto.getMail());
		return customer;
	}

	@Override
	public String toString() {
		return "Customer [date=" + date + ", firstName=" + firstName + ", id=" + id + ", lastName=" + lastName
				+ ", location=" + location + ", mail=" + mail + ", rating=" + rating + ", telNum=" + telNum + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((location == null) ? 0 : location.hashCode());
		result = prime * result + ((mail == null) ? 0 : mail.hashCode());
		long temp;
		temp = Double.doubleToLongBits(rating);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((telNum == null) ? 0 : telNum.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (id != other.id)
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (location == null) {
			if (other.location != null)
				return false;
		} else if (!location.equals(other.location))
			return false;
		if (mail == null) {
			if (other.mail != null)
				return false;
		} else if (!mail.equals(other.mail))
			return false;
		if (Double.doubleToLongBits(rating) != Double.doubleToLongBits(other.rating))
			return false;
		if (telNum == null) {
			if (other.telNum != null)
				return false;
		} else if (!telNum.equals(other.telNum))
			return false;
		return true;
	}

}