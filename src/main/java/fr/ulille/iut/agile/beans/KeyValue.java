package fr.ulille.iut.agile.beans;

public class KeyValue {

    private String location;
    private String value;

    public KeyValue(String location, String value) {
        this.location = location;
        this.value = value;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
