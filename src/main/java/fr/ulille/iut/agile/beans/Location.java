package fr.ulille.iut.agile.beans;

import fr.ulille.iut.agile.dto.LocationCreateDto;
import fr.ulille.iut.agile.dto.LocationDto;

public class Location {
    private int id;
    private String places;
    private String coord;
    private String date;
    private String infos;
    private String hours;

    public Location() {
    }

    public Location(String date, String hours) {
        this.date = date;
        this.hours = hours;
    }

    public Location(String places, String coord, String date, String infos, String hours) {
        this.places = places;
        this.coord = coord;
        this.date = date;
        this.infos = infos;
        this.hours = hours;
    }

    public Location(int id, String places, String coord, String date, String infos, String hours) {
        this.id = id;
        this.places = places;
        this.coord = coord;
        this.date = date;
        this.infos = infos;
        this.hours = hours;
    }

    public static LocationDto toDto(Location c) {
        LocationDto dto = new LocationDto();
        dto.setId(c.getId());
        dto.setCoord(c.getCoord());
        dto.setDate(c.getDate());
        dto.setHours(c.getHours());
        dto.setInfos(c.getInfos());
        dto.setPlaces(c.getPlaces());
        return dto;
    }

    public static Location fromDto(LocationDto dto) {
        Location c = new Location();
        c.setId(dto.getId());
        c.setCoord(dto.getCoord());
        c.setDate(dto.getDate());
        c.setHours(dto.getHours());
        c.setInfos(dto.getInfos());
        c.setPlaces(dto.getPlaces());

        return c;
    }

    public static LocationCreateDto toCreateDto(Location location) {
        LocationCreateDto dto = new LocationCreateDto();
        dto.setDate(location.getDate());
        dto.setHours(location.getHours());
        return dto;
    }

    public static Location fromLocationCreateDto(LocationCreateDto dto) {
        Location location = new Location();
        location.setDate(dto.getDate());
        location.setHours(dto.getHours());

        return location;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPlaces() {
        return places;
    }

    public void setPlaces(String places) {
        this.places = places;
    }

    public String getCoord() {
        return coord;
    }

    public void setCoord(String coord) {
        this.coord = coord;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getInfos() {
        return infos;
    }

    public void setInfos(String infos) {
        this.infos = infos;
    }

    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }

    @Override
    public String toString() {
        return "Location{" + "id=" + id + ", places='" + places + '\'' + ", coord='" + coord + '\'' + ", date='" + date
                + '\'' + ", infos='" + infos + '\'' + ", hours='" + hours + '\'' + '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Location other = (Location) obj;
        if (coord == null) {
            if (other.coord != null)
                return false;
        } else if (!coord.equals(other.coord))
            return false;
        if (date == null) {
            if (other.date != null)
                return false;
        } else if (!date.equals(other.date))
            return false;
        if (hours == null) {
            if (other.hours != null)
                return false;
        } else if (!hours.equals(other.hours))
            return false;
        if (id != other.id)
            return false;
        if (infos == null) {
            if (other.infos != null)
                return false;
        } else if (!infos.equals(other.infos))
            return false;
        if (places == null) {
            if (other.places != null)
                return false;
        } else if (!places.equals(other.places))
            return false;
        return true;
    }

}