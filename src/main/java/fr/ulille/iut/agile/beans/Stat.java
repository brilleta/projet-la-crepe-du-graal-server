package fr.ulille.iut.agile.beans;

import fr.ulille.iut.agile.dto.StatDto;

public class Stat {

    private String statName;
    private double statValue;

    public Stat(String statName, int statValue) {
        this.statName = statName;
        this.statValue = statValue;
    }

    public Stat() {
    }

    public String getStatName() {
        return statName;
    }

    public void setStatName(String statName) {
        this.statName = statName;
    }

    public double getStatValue() {
        return statValue;
    }

    public void setStatValue(double statValue) {
        this.statValue = statValue;
    }

    @Override
    public String toString() {
        return "Stat [statName=" + statName + ", statValue=" + statValue + "]";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Stat other = (Stat) obj;
        if (statName == null) {
            if (other.statName != null)
                return false;
        } else if (!statName.equals(other.statName))
            return false;
        if (statValue != other.statValue)
            return false;
        return true;
    }

    public static StatDto toDto(Stat c) {
        StatDto dto = new StatDto();
        dto.setStatName(c.getStatName());
        dto.setStatValue(c.getStatValue());
        return dto;
    }

    public static Stat fromDto(StatDto dto) {
        Stat c = new Stat();
        c.setStatName(dto.getStatName());
        c.setStatValue(dto.getStatValue());
        return c;
    }

}