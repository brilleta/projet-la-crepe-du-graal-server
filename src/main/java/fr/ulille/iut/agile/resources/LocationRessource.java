package fr.ulille.iut.agile.resources;

import fr.ulille.iut.agile.BDDFactory;
import fr.ulille.iut.agile.beans.Location;
import fr.ulille.iut.agile.dao.LocationsDao;
import fr.ulille.iut.agile.dto.LocationDto;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Path("/locations")
public class LocationRessource {
    private static final Logger LOGGER = Logger.getLogger(LocationRessource.class.getName());
    @Context
    public UriInfo uriInfo;
    private LocationsDao locations;

    public LocationRessource() {
        locations = BDDFactory.buildDao(LocationsDao.class);
        locations.createsTable();
    }


    @GET
    public List<LocationDto> getAll() {
        LOGGER.info("LocationResource:getAll");
        List<LocationDto> l = locations.getAll().stream().map(Location::toDto).collect(Collectors.toList());
        return l;
    }

    @GET
    @Path("/get/{id}")
    public Response getbyId(@PathParam("id") int id) {
        LOGGER.info("LocationResource:getOne : "+id);
        Location l = locations.finfById(id);
        return Response.ok(l).build();
    }

    @POST
    @Consumes("application/x-www-form-urlencoded")
    public Response createLocation(@FormParam("places") String place, @FormParam("coord") String coord,
                                   @FormParam("date") String date, @FormParam("infos") String infos, @FormParam("hours") String hours) {
        LOGGER.info("LocationResource:create");
        Location l = new Location(place, coord, date, infos, hours);
        locations.insertGlobal(l);
        return Response.ok(l).build();
    }

    @POST
    @Path("update/{id}")
    @Consumes("application/x-www-form-urlencoded")
    public Response updateLocation(@PathParam("id") int id, @FormParam("places") String place, @FormParam("coord") String coord,
                                   @FormParam("date") String date, @FormParam("infos") String info, @FormParam("hours") String hours) {
        LOGGER.info("LocationResource:update");
        Location l = new Location(id,place,coord,date,info,hours);
        System.out.println(l.toString());
        locations.updateLocation(l);
        return Response.ok(l).build();
    }

    @GET
    @Path("delete/{place}")
    public Response delete(@PathParam("place") String s) {
        LOGGER.info("LocationResource:delete");
        Location l = locations.finfByPlace(s);
        locations.remove(l);
        return Response.ok(l).build();
    }


}