package fr.ulille.iut.agile.resources;

import fr.ulille.iut.agile.BDDFactory;
import fr.ulille.iut.agile.beans.KeyValue;
import fr.ulille.iut.agile.beans.Stat;
import fr.ulille.iut.agile.dao.StatsDao;
import fr.ulille.iut.agile.dto.StatDto;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Path("/stats")
public class StatRessource {

    private static final Logger LOGGER = Logger.getLogger(fr.ulille.iut.agile.resources.CustomerResource.class.getName());
    @Context
    public UriInfo uriInfo;
    private StatsDao stats;

    public StatRessource() {
        stats = BDDFactory.buildDao(StatsDao.class);
    }

    @GET
    @Path("location")
    public List<StatDto> getLocationStat() {
        LOGGER.info("getLocationStat()");
        try {
            List<StatDto> stat = stats.getLocationStat().stream().map(Stat::toDto).collect(Collectors.toList());
            return stat;
        } catch (Exception e) {
            // Cette exception générera une réponse avec une erreur 404
            e.printStackTrace();
        }
        return null;
    }

    @GET
    @Path("rating")
    public List<StatDto> getRatingStat() {
        LOGGER.info("getLocationStat()");
        try {
            List<StatDto> stat = stats.getRatingStat().stream().map(Stat::toDto).collect(Collectors.toList());
            return stat;
        } catch (Exception e) {
            // Cette exception générera une réponse avec une erreur 404
            e.printStackTrace();
        }
        return null;
    }

    @GET
    @Path("rating/location")
    public Response getRatingStatPerLocation() {
        LOGGER.info("getRatingStatPerLocation()");
        List<StatDto> ls = stats.getRectDao().stream().map(Stat::toDto)
                .collect(Collectors.toList());
        return Response.ok(ls).build();
    }

}