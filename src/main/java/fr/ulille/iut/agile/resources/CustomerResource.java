package fr.ulille.iut.agile.resources;

import fr.ulille.iut.agile.BDDFactory;
import fr.ulille.iut.agile.beans.Customer;
import fr.ulille.iut.agile.dao.CustomersDao;
import fr.ulille.iut.agile.dao.LocationsDao;
import fr.ulille.iut.agile.dto.CustomerCreateDto;
import fr.ulille.iut.agile.dto.CustomerDto;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Path("/customers")
public class CustomerResource {
    private static final Logger LOGGER = Logger.getLogger(CustomerResource.class.getName());
    @Context
    public UriInfo uriInfo;
    private CustomersDao customers;

    public CustomerResource() {
        customers = BDDFactory.buildDao(CustomersDao.class);
        customers.createTable();
    }

    @GET
    public List<CustomerDto> getAll() {
        LOGGER.info("CustomerResource:getAll");
        List<CustomerDto> l = customers.getAll().stream().map(Customer::toDto).collect(Collectors.toList());
        return l;
    }

    @GET
    @Path("/location/{id}")
    @Produces("application/json")
    public Response getWithLocation(@PathParam("id") int id) {
        LOGGER.info("getWithLocation(" + id + ")");
        LocationsDao locations = BDDFactory.buildDao(LocationsDao.class);
        String l = locations.finfById(id).getPlaces();
        List<Customer> cdto = customers.getWithLocation(l);
        return Response.ok(cdto).build();
    }

    @GET
    @Path("/mail/{id}")
    @Produces("application/json")
    public Response getMailWithId(@PathParam("id") int id) {
        LOGGER.info("getMailWithId(" + id + ")");
        LocationsDao locations = BDDFactory.buildDao(LocationsDao.class);
        String l = locations.finfById(id).getPlaces();
        List<Customer> cdtos = customers.getWithLocation(l);
        String s = "[";
        for (Customer c : cdtos) {
            if (!Customer.toDto(c).toStringMailingFormat().equals(Customer.toDto(cdtos.get(cdtos.size() - 1)).toStringMailingFormat()))
                s += Customer.toDto(c).toStringMailingFormat() + ",";
            else
                s += Customer.toDto(c).toStringMailingFormat();
        }
        s += "]";
        return Response.ok(s).build();
    }

    @GET
    @Path("{id}")
    public CustomerDto getOneCustomer(@PathParam("id") long id) {
        LOGGER.info("getOneCustomer(" + id + ")");
        try {
            Customer customer = customers.findById(id);
            return Customer.toDto(customer);
        } catch (Exception e) {
            // Cette exception générera une réponse avec une erreur 404
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
    }

    @GET
    @Path("{id}/rating")
    public Double getCustomerRating(@PathParam("id") long id) {
        LOGGER.info("getCustomerRating(" + id + ")");
        try {
            Double rating = customers.getRating(id);
            return rating;
        } catch (Exception e) {
            // Cette exception générera une réponse avec une erreur 404
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }

    }

    @PUT
    @Path("{id}/rating")
    @Consumes("application/x-www-form-urlencoded")
    public CustomerDto setCustomerRating(@PathParam("id") long id, @FormParam("rating") Double rating) {
        LOGGER.info("setCustomerRating(" + id + ")");
        try {
            Customer customer = customers.findById(id);
            customer.setRating(rating);
            customers.updateRating(customer);
            return Customer.toDto(customer);
        } catch (Exception e) {
            // Cette exception générera une réponse avec une erreur 404
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }

    }

    @PUT
    @Path("{id}/rating")
    public CustomerDto setCustomerRating(@PathParam("id") long id, CustomerDto customerDto) {
        LOGGER.info("setCustomerRating(" + id + ")");
        try {
            Customer customer = Customer.fromDto(customerDto);
            customer.setId(id);
            System.out.println(customer);
            customers.updateRating(customer);
            return Customer.toDto(customer);
        } catch (Exception e) {
            // Cette exception générera une réponse avec une erreur 404
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }

    }

    @PUT
    @Path("{id}")
    @Consumes("application/x-www-form-urlencoded")
    public CustomerDto updateCustomer(@PathParam("id") long id, @FormParam("firstName") String firstName,
            @FormParam("lastName") String lastName, @FormParam("date") String date, @FormParam("telNum") String telNum,
            @FormParam("mail") String mail, @FormParam("location") String location) {
        LOGGER.info("updateCustomer ");

        Customer existing = customers.findById(id);
        System.out.println(existing);
        if (existing != null) {
            Customer customer = existing;
            customer.setFirstName(firstName);
            customer.setLastName(lastName);
            customer.setDate(date);
            customer.setMail(mail);
            customer.setTelNum(telNum);
            customer.setLocation(location);
            customers.updateCustomer(customer);

            CustomerDto customerDto = Customer.toDto(customer);

            return customerDto;
        }
        return null;

    }

    @POST
    @Consumes("application/x-www-form-urlencoded")
    public CustomerDto createCustomer(@FormParam("firstName") String firstName, @FormParam("lastName") String lastName,
            @FormParam("date") String date, @FormParam("telNum") String telNum, @FormParam("mail") String mail,
            @FormParam("location") String location) {
        LOGGER.info("Creating customer ");
        Customer existing = customers.findByName(lastName, firstName);
        System.out.println(existing);
        if (existing != null) {
            throw new WebApplicationException(Response.Status.CONFLICT);
        } else {
            Customer customer = new Customer();
            customer.setFirstName(firstName);
            customer.setLastName(lastName);
            customer.setDate(date);
            customer.setMail(mail);
            customer.setTelNum(telNum);
            customer.setLocation(location);

            long id = customers.insert(customer);
            customer.setId(id);
            CustomerDto customerDto = Customer.toDto(customer);

            return customerDto;
        }
    }

    @POST
    public CustomerDto createCustomer(CustomerCreateDto inputCustomer) {
        LOGGER.info("Creating customer : ");
        Customer existing = customers.findByName(inputCustomer.getLastName(), inputCustomer.getFirstName());
        if (existing != null) {
            throw new WebApplicationException(Response.Status.CONFLICT);
        }

        Customer customer = Customer.fromCustomerCreateDto(inputCustomer);
        LOGGER.info("Create customer " + customer);
        long id = customers.insert(customer);
        customer.setId(id);
        CustomerDto customerDto = Customer.toDto(customer);

        return customerDto;
    }

    @PUT
    @Path("{id}")
    public CustomerDto updateCustomer(@PathParam("id") long id, CustomerCreateDto inputCustomer) {
        LOGGER.info("Creating customer : ");
        Customer existing = customers.findById(id);
        if (existing != null) {
            throw new WebApplicationException(Response.Status.CONFLICT);
        }

        Customer customer = Customer.fromCustomerCreateDto(inputCustomer);
        LOGGER.info("Create customer " + customer);
        customers.updateCustomer(customer);
        customer.setId(id);
        CustomerDto customerDto = Customer.toDto(customer);

        return customerDto;
    }

    @GET
    @Path("nombre")
    @Produces("application/json")
    public Response nombreDeClient() {
        LOGGER.info("nb client");
        Integer nb = customers.countClient();
        String json = "[{\"statName\":\"nombre\",\"statValue\":" + nb + "}]";
        return Response.ok(json).build();
    }

    @GET
    @Path("moyenneglobal")
    @Produces("application/json")
    public Response moyenneGlobal() {
        LOGGER.info("moyenne global");
        Double nb = customers.moyenneGlobal();
        String json = "[{\"statName\":\"moyenneglobal\",\"statValue\":" + nb + "}]";
        return Response.ok(json).build();
    }

}