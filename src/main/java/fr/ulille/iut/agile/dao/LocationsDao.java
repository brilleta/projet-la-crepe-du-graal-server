package fr.ulille.iut.agile.dao;

import fr.ulille.iut.agile.beans.Location;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

public interface LocationsDao {

    @SqlUpdate("Create table if not exists locations(" + "id integer primary key," + "places varchar(30) ,"
            + "coord varchar(30)," + "date varchar(3)," + "infos varchar(80)," + "hours varchar(20))")
    void createsTable();

    @SqlUpdate("Insert into locations(date,hours) values (:date, :hours)")
    @GetGeneratedKeys
    long insert(@BindBean() Location location);

    //
    @SqlUpdate("Insert into locations(places,coord,date,infos,hours) values (:places, :coord, :date, :infos, :hours)")
    @GetGeneratedKeys
    long insertGlobal(@BindBean() Location location);

    @SqlQuery("Select * from locations order by places")
    @RegisterBeanMapper(Location.class)
    List<Location> getAll();

    @SqlUpdate("Drop table if exists Locations")
    void dropLocationsTable();

    @SqlUpdate("Delete from locations where date = :date AND hours = :hours")
    void remove(@BindBean() Location location);

    @SqlQuery("Select * from locations where places = :pl ")
    @RegisterBeanMapper(Location.class)
    Location finfByPlace(@Bind("pl") String pl);

    @SqlQuery("Select * from locations where id = :id ")
    @RegisterBeanMapper(Location.class)
    Location finfById(@Bind("id") int id);

    @SqlUpdate("Update locations SET places = :places , coord = :coord , date = :date , infos = :infos , hours = :hours where id = :id")
    @RegisterBeanMapper(Location.class)
    void updateLocation(@BindBean() Location location);

}