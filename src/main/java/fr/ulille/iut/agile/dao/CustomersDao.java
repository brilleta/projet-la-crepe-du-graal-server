package fr.ulille.iut.agile.dao;

import java.util.List;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import fr.ulille.iut.agile.beans.Customer;
import fr.ulille.iut.agile.beans.Stat;

public interface CustomersDao {

	@SqlUpdate("Create table if not exists customers(" + "id integer primary key," + "lastName varchar(30) not null,"
			+ "firstName varchar(30) not null," + "location varchar(50) not null," + "mail varchar(80) not null,"
			+ "date varchar(14) not null," + "telNum varchar(14) not null," + "rating double(10,9) default 0, "
			+ "Constraint unique_customers Unique(lastName, firstName) )")
	void createTable();

	@SqlUpdate("Insert into customers(lastName, firstName,location,mail,date,telNum) values (:lastName, :firstName, :location,:mail,:date,:telNum)")
	@GetGeneratedKeys
	long insert(@BindBean() Customer customer);

	@SqlQuery("Select * from customers")
	@RegisterBeanMapper(Customer.class)
	List<Customer> getAll();

	@SqlQuery("Select * from customers where id = :id")
	@RegisterBeanMapper(Customer.class)
	Customer findById(@Bind("id") long id);

	@SqlQuery("Select * from customers where location = :l")
	@RegisterBeanMapper(Customer.class)
	List<Customer> getWithLocation(@Bind("l") String l);

	@SqlQuery("Select rating from customers where id = :id and showRating!=0")
	@RegisterBeanMapper(Customer.class)
	Double getRating(@Bind("id") long id);

	@SqlQuery("Select * from customers where lastName = :lastName AND firstName = :firstName")
	@RegisterBeanMapper(Customer.class)
	Customer findByName(@Bind("lastName") String lastName, @Bind("firstName") String firstName);

	@SqlUpdate("Drop table if exists customers")
	void dropCustomersTable();

	@SqlUpdate("Delete from customers where id = :id")
	void remove(@Bind("id") long id);

	@SqlUpdate("Update customers set firstName = :firstName, lastName = :lastName,mail = :mail, date = :date, location = :location,telNum = :telNum  where id = :id")
	@RegisterBeanMapper(Customer.class)
	void updateCustomer(@BindBean() Customer customer);

	@SqlUpdate("Update customers set rating = :rating where id = :id")
	@RegisterBeanMapper(Customer.class)
	void updateRating(@BindBean() Customer customer);

	@SqlQuery("Select count (*) from customers")
	Integer countClient();

	@SqlQuery("Select AVG(rating) from customers")
	Double moyenneGlobal();

}
