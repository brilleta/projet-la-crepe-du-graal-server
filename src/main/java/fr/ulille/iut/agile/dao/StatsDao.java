package fr.ulille.iut.agile.dao;

import fr.ulille.iut.agile.beans.KeyValue;
import fr.ulille.iut.agile.beans.Stat;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.statement.SqlQuery;

import java.util.List;

public interface StatsDao {

    @SqlQuery("Select locations.places as statName,count(customers.location) as statValue from locations left join customers on locations.places=customers.location group by locations.places")
    @RegisterBeanMapper(Stat.class)
    List<Stat> getLocationStat();

    @SqlQuery("Select rating as statName,count(*) as statValue from  customers group by rating")
    @RegisterBeanMapper(Stat.class)
    List<Stat> getRatingStat();

    @SqlQuery("Select locations.places as statName,AVG(customers.rating) as statValue from locations left join customers on locations.places=customers.location  group by locations.places")
    @RegisterBeanMapper(Stat.class)
    List<Stat> getRatingStatPerLocation();

    //
    @SqlQuery("SELECT location as statName, AVG(rating) as statValue FROM customers where rating > 0 group by location")
    @RegisterBeanMapper(Stat.class)
    List<Stat> getRectDao();

}
