package fr.ulille.iut.agile.dto;

public class LocationCreateDto {
    private String date;
    private String hours;

    public LocationCreateDto() {
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }
}