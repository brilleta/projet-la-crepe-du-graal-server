package fr.ulille.iut.agile.dto;

public class CustomerCreateDto {

	private String lastName;
	private String firstName;
	private String location;
	private String telNum;
	private String date;
	private String mail;

	public CustomerCreateDto() {
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getTelNum() {
		return telNum;
	}

	public void setTelNum(String telNum) {
		this.telNum = telNum;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	@Override
	public String toString() {
		return "CustomerCreateDto [date=" + date + ", firstName=" + firstName + ", lastName=" + lastName + ", location="
				+ location + ", mail=" + mail + ", telNum=" + telNum + "]";
	}

}