package fr.ulille.iut.agile.dto;

public class StatDto {

    private String statName;
    private double statValue;

    public StatDto(String statName, double statValue) {
        this.statName = statName;
        this.statValue = statValue;
    }

    public StatDto() {
    }

    public String getStatName() {
        return statName;
    }

    public void setStatName(String statName) {
        this.statName = statName;
    }

    public double getStatValue() {
        return statValue;
    }

    public void setStatValue(double statValue) {
        this.statValue = statValue;
    }

}