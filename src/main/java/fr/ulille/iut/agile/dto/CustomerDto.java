package fr.ulille.iut.agile.dto;

public class CustomerDto {

    private long id;
    private String lastName;
    private String firstName;
    private String date;
    private String telNum;
    private String location;
    private String mail;
    private double rating;

    public CustomerDto() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTelNum() {
        return telNum;
    }

    public void setTelNum(String telNum) {
        this.telNum = telNum;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public String toStringMailingFormat() {
        return "{" + '\"' + "lastName" + '\"' + ':' + '\"' + lastName + '\"' + "," + "\"firstName"+ '\"' + ':' + '\"' + firstName + '\"'
                + ",\"mail\":\"" + mail + '\"' + '}';
    }
}