package fr.ulille.iut.agile.dto;

public class LocationDto {
    private int id;
    private String places;
    private String coord;
    private String date;
    private String infos;
    private String hours;

    public LocationDto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPlaces() {
        return places;
    }

    public void setPlaces(String places) {
        this.places = places;
    }

    public String getCoord() {
        return coord;
    }

    public void setCoord(String coord) {
        this.coord = coord;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getInfos() {
        return infos;
    }

    public void setInfos(String infos) {
        this.infos = infos;
    }

    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }

    @Override
    public String toString() {
        return "LocationDto{" +
                "id=" + id +
                ", places='" + places + '\'' +
                ", coord='" + coord + '\'' +
                ", date='" + date + '\'' +
                ", infos='" + infos + '\'' +
                ", hours='" + hours + '\'' +
                '}';
    }
}