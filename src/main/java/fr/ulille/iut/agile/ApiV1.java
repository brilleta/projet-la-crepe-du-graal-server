package fr.ulille.iut.agile;

import fr.ulille.iut.agile.beans.Customer;
import fr.ulille.iut.agile.beans.Location;
import fr.ulille.iut.agile.dao.CustomersDao;
import fr.ulille.iut.agile.dao.LocationsDao;
import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.ApplicationPath;
import java.util.logging.Logger;

@ApplicationPath("api/v1/")
public class ApiV1 extends ResourceConfig {

    private static final Logger LOGGER = Logger.getLogger(ApiV1.class.getName());

    public ApiV1() {
        LOGGER.info("Initialize database");
        CustomersDao customersDao = BDDFactory.buildDao(CustomersDao.class);
        LocationsDao locationsDao = BDDFactory.buildDao(LocationsDao.class);
        customersDao.dropCustomersTable();
        customersDao.createTable();
        customersDao.insert(new Customer("Shepard", "John", "17/05/1901", "0456894575",
                "Leroy Merlin à Lesquin", "aze@mail.com"));
        customersDao.insert(
                new Customer("Doe", "Jane", "17/05/1901", "0456894575", "Bouvines", "aze@mail.com"));
        customersDao.insert(new Customer("Dupont", "Francoise", "17/05/1901", "0456894575", "Bouvines",
                "aze@mail.com"));
        customersDao.insert(new Customer("Jane", "Francois", "17/05/1901", "0456894575",
                "Business Pôle des Près", "aze@mail.com"));
        customersDao.insert(new Customer("Rocky", "Balbenoit", "17/05/1901", "0456894575",
                "Business Pôle des Près", "aze@mail.com"));
        customersDao.insert(new Customer("Jai", "Pasmal", "17/05/1901", "0456894575", "Business Pôle des Près",
                "aze@mail.com"));
        initLocation(locationsDao);

    }

    public void initLocation(LocationsDao locationsDao) {
        locationsDao.dropLocationsTable();
        locationsDao.createsTable();
        locationsDao.insertGlobal(new Location(1,"Leroy Merlin à Lesquin", "50.585928,3.089046", "Mon",
                "Lundi midi (12h - 13h45)\n51, route de Douai LESQUIN", "12:00:00 13:45:00"));
        locationsDao.insertGlobal(new Location(2,"Bouvines", "50.578324,3.188864", "Mon",
                "Lundi soir (18h30 - 21h)\n527, rue Félix Dehau BOUVINES", "18:30:00 21:00:00"));
        locationsDao.insertGlobal(new Location(3,"Business Pôle des Près", "50.649233,3.128401", "Tue",
                "Mardi midi (12h - 13h45)\nAngles rue Papin et rue Denis Papin VILLENEUVE D\'ASCQ",
                "12:00:00 13:45:00"));
        locationsDao.insertGlobal(new Location(4,"Match à Cysoing", "50.567057,3.223774", "Tue",
                "Mardi soir (18h30 - 21h)\nRue Félix Demesmay CYSOING", "18:30:00 21:00:00"));
        locationsDao.insertGlobal(new Location(5,"Parc du Moulin à Wambrechies (Bulteau)", "50.685622,3.060190",
                "Thu",
                "Jeudi midi (12h - 13h45)\nParking Holding Bulteau : 435 wambrechies WAMBRECHIES",
                "12:00:00 13:45:00"));
        locationsDao.insertGlobal(new Location(6,"Marché de Lesquin", "50.591754,3.104891", "Thu",
                "Jeudi soir (18h30 - 21h)\n59, avenue du Docteur Schweitzer LESQUIN",
                "18:30:00 21:00:00"));
        locationsDao.insertGlobal(new Location(7,"La Haute Borne à Villeneuve d'Ascq", "50.606446,3.156577",
                "Fri", "Vendredi midi (12h - 13h45)\nParking d\'Econocom, rue Horus VILLENEUVE D\'ASCQ",
                "12:00:00 13:45:00"));
    }
}
